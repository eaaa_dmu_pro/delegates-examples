﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesExample02
{
    class Program
    {
        delegate T BinaryOperand<in S, out T>(S arg1, S arg2);

        static BinaryOperand<int, int> intFunction = division;

        static BinaryOperand<decimal, decimal> decimalFunction = division;

        static void Main(string[] args)
        {
            Console.WriteLine(intFunction(16, 3));
            Console.WriteLine(decimalFunction(16, 3));
            Console.Read();
        }

        static int division(int dividend, int divisor)
        {
            if (divisor == 0)
            {
                throw new ArgumentException("Divisor must be non-zero");
            }
            return dividend / divisor;
        }


        static decimal division(decimal dividend, decimal divisor)
        {
            if (divisor == 0)
            {
                throw new ArgumentException("Divisor must be non-zero");
            }
            return dividend / divisor;
        }

    }
}
