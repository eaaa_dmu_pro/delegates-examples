﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesExample
{
    class Program
    {
        delegate int BinaryOperator(int arg1, int arg2);

        static void Main(string[] args)
        {
            BinaryOperator del = Addition;
            PrintResult(del, 3, 5);

            PrintResult(Subtraction, 10, 5);

            Console.WriteLine(del(11, 6));
            Console.Read();
        }

        static void PrintResult(BinaryOperator op, int arg1, int arg2)
        {
            Console.WriteLine(op.Invoke(arg1, arg2));
        }

        static int Addition(int augend, int addend)
        {
            return augend + addend;
        }

        static int Subtraction(int minuend, int subtrahend)
        {
            return minuend - subtrahend;
        }
    }
}
